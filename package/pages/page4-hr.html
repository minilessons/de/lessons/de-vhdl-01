<p>Na kolegiju <em>Digitalna logika</em> za modeliranje digitalnih sklopova nećemo koristiti tip <code>bit</code>
jer isti ne omogućava modeliranje sklopova s kojima se susrećemo u praksi kao niti njihovo realnije simuliranje.
</p>

<p>Primjerice, osim što izlaz digitalnog sklopa može biti u logičkoj nuli odnosno u logičkoj jedinici, postoje izvedbe
digitalnih sklopova kod kojih je izlaz moguće prebaciti u stanje visoke impedancije (takozvano "treće stanje") čime se
taj sklop odvaja od ostatka sklopova i na njih više ne utječe. Nadalje, moguća je situacija kod koje su izlazi dva sklopa
kratko spojeni, pri čemu jedan od njih pokušava na izlazu generirati logičku nulu a drugi logičku jedinicu. Takve i slične
situacije moramo nekako modelirati pa nam skup vrijednosti {<code>'0'</code>, <code>'1'</code>} više nije dovoljan: trebamo
još mogućih vrijednosti.
</p>

<p>Danas se zbog toga digitalni sklopovi modeliraju uporabom podatkovnog tipa <code>std_logic</code> ("standardni tip za digitalnu logiku").
   Taj tip podataka definiran je u biblioteci <code>ieee</code> i to u njezinom paketu <code>std_logic_1164</code>. Stoga
   ćemo prilikom opisivanja digitalnih sklopova uvijek morati uključiti biblioteku <code>ieee</code> te uvesti sve
   definicije iz njezinog paketa <code>std_logic_1164</code>.
</p>

<div class="mlImportant">
<p><strong>Zapamtite!</strong> Da bismo pri modeliranju digitalnog sklopovlja mogli koristiti tip <code>std_logic</code> odnosno njegovu višebitnu inačicu <code>std_logic_vector</code>,
   morat ćemo u opis uključiti biblioteku <code>ieee</code> te uvesti sve definicije iz njezinog paketa <code>std_logic_1164</code>.
</p>
</div>

<p>
  Tip <code>std_logic</code> definira 9 vrijednosti koje signal odnosno varijabla ovog tipa može poprimiti.
  Vrijednosti kao i kratak opis navedene su u nastavku.
</p>

<div class="mlTableCaption"><span class="mlTblKey">Tablica 1.</span> Vrijednosti definirane tipom <code>std_logic</code>.</div>
<table class="mlTableCC mlTableVRl1 mlTableCC2 mlTableCCC">
<thead>
<tr><th>Vrijednost</th><th>Opis</th></tr>
</thead>
<tbody>
<tr>
  <td><code>'0'</code></td>
  <td>
    Predstavlja logičku nulu (takozvana "jaka nula"). Konceptualno, možemo zamisliti da je
    izlaz sklopa uključenim tranzistorom pritegnut na masu.
  </td>
</tr>
<tr>
  <td><code>'1'</code></td>
  <td>
    Predstavlja logičku jedinicu (takozvana "jaka jedinica"). Konceptualno, možemo zamisliti da je
    izlaz sklopa uključenim tranzistorom pritegnut na napajanje.
  </td>
</tr>
<tr>
  <td><code>'L'</code></td>
  <td>
    Predstavlja pasivno generiranu logičku nulu (takozvana "slaba nula"). Konceptualno, možemo zamisliti da je
    izlaz sklopa preko otpornika pritegnut na masu.
  </td>
</tr>
<tr>
  <td><code>'H'</code></td>
  <td>
    Predstavlja pasivno generiranu logičku jedinicu (takozvana "slaba jedinica"). Konceptualno, možemo zamisliti da je
    izlaz sklopa preko otpornika pritegnut na napajanje.
  </td>
</tr>
<tr>
  <td><code>'X'</code></td>
  <td>
    Predstavlja stanje koje se dobije kada se dva aktivno generirana izlaza kratko spoje
    a sklopovi generiraju komplementarne izlaze (u praksi, označava nastanak kratkog
    spoja od napajanja prema masi, što može dovesti do uništenja sklopa). 
    Naziv ove vrijednosti je "jako-nepoznato".
  </td>
</tr>
<tr>
  <td><code>'W'</code></td>
  <td>
    Predstavlja stanje koje se dobije kada se dva pasivno generirana izlaza kratko spoje
    a sklopovi generiraju komplementarne izlaze (u praksi, u električkom se smislu ne mora
    dogoditi ništa fatalnoga poput pregaranja sklopa, ali napon koji će biti na izlazu najvjerojatnije neće
    biti niti u području logičke nule niti u području logičke jedinice - ovisit će o iznosima priteznih otpornika.
    Naziv ove vrijednosti je "slabo-nepoznato".
  </td>
</tr>
<tr>
  <td><code>'Z'</code></td>
  <td>
    Predstavlja izlaz koji je i od napajanja i od mase izoliran velikim otporom; to je izlaz koji je
    u trećem stanju (stanju visoke impedancije).
  </td>
</tr>
<tr>
  <td><code>'-'</code></td>
  <td>
    Predstavlja situaciju u kojoj nas nije briga što je na izlazu (engl. <em>don't care</em>).
  </td>
</tr>
<tr>
  <td><code>'U'</code></td>
  <td>
    Predstavlja situaciju u kojoj simulator ne zna što je na izlazu (engl. <em>uninitialized</em>).
  </td>
</tr>
</tbody>
</table>


<p>Posljednje navedena vrijednost <code>'U'</code> zahtjeva dodatno pojašnjenje. Pretpostavite da modeliramo 
logički sklop ILI tako da ima kašnjenje od 10 nanosekundi. Neka na početku
simulacije (u trenutku <em>t = 0</em>) na njegove ulaze dovedemo vrijednosti koje odgovaraju logičkoj nuli.
Što očekujemo od simulatora da nam kaže za izlaz tog sklopa u trenutku <em>t = 0</em> ili u trenutku <em>t = 1 ns</em>?
Kako god da radi simulaciju, simulator će temeljem našeg opisa moći
zaključiti da će od desete nanosekunde na izlazu biti vrijednost nula; međutim, što će biti od
trenutka <em>t = 0 ns</em> do <em>t = 10 ns</em> - na to neće moći odgovoriti. Upravo u tom periodu,
simulator će izlazu pridijeliti vrijednost <code>'U'</code> čime će nas obavijestiti da taj izlaz još nije inicijaliziran
i nema poznatu vrijednost.</p>

<p>Kod tipičnih opisa digitalnog sklopovlja, vrijednosti <code>'U'</code> pojavljivat će se na samom početku simulacije
i nakon kraćeg vremena će nestati.</p>

<p>Višebitne vrijednosti definirane su tipom <code>std_logic_vector</code> i pišu se pod dvostrukim navodnicima. Primjerice:
<code>"110UX"</code> predstavlja petbitnu vrijednost čije su komponente redom <code>'1'</code>, <code>'1'</code>, 
<code>'0'</code>, <code>'U'</code> i <code>'X'</code>.
</p>

<p>
Nad tipom <code>std_logic</code> definiran je isti skup operatora koje možemo primjenjivati: unarni <code>not</code>
te binarni operatori <code>and</code>, <code>or</code>, <code>nand</code>, <code>nor</code>, <code>xor</code> i <code>xnor</code>.
Djelovanje svakog od binarnih operatora specificirano je tablicom koja sadrži 81 element: prvi argument može poprimiti
bilo koju od devet mogućih vrijednosti (od <code>'0'</code> do <code>'U'</code>) i drugi argument može poprimiti bilo koju od devet 
mogućih vrijednosti (od <code>'0'</code> do <code>'U'</code>). Time imamo
\(9 \times 9 = 81\) mogućih ulaznih vrijednosti za binarni operator i za svaku takvu kombinaciju moramo znati što će biti
rezultat djelovanja operatora. Primjerice, možemo se pitati koji je rezultat <code>'0' and 'Z'</code>, <code>'U' and 'X'</code>
i slično.
</p>

<p>U okviru kolegija <em>Digitalna logika</em> morate znati kako osnovni logički operatori djeluju nad tročlanim
skupom {<code>'0'</code>, <code>'1'</code>, <code>'U'</code>}. Sve operacije bit će zatvorene nad tim skupom što znači
da je rezultat djelovanja operatora opet neka od vrijednosti iz tog skupa.</p>

<div class="mlImportant">
<p>U okviru kolegija <em>Digitalna logika</em> <strong>morate</strong> znati kako osnovni logički operatori djeluju nad tročlanim
skupom {<code>'0'</code>, <code>'1'</code>, <code>'U'</code>}.</p>
</div>

<p>Pogledajmo najprije kako operator negacije djeluje nad skupom: {<code>'0'</code>, <code>'1'</code>, <code>'U'</code>}.</p>

<p>
 <code>not '0' = '1'</code><br>
 <code>not '1' = '0'</code><br>
 <code>not 'U' = 'U'</code>
</p>

<p>
  Posljednji slučaj postaje jasan ako se prisjetimo što znači da je signal vrijednosti <code>'U'</code>: to znači
  da ne znamo koja je njegova stvarna vrijednost. Kada bi ona bila <code>'0'</code>, rezultat bi bio <code>'1'</code>.
  Kada bi ona bila <code>'1'</code>, rezultat bi bio <code>'0'</code>. No kako ne znamo koja je stvarna vrijednost signala,
  a kako rezultat ovisi o toj vrijednosti, jedino što možemo zaključiti jest da ne znamo niti koja je stvarna vrijednost 
  na izlazu negacije; a tu situaciju znamo opisati: kažemo da je rezultat jednak <code>'U'</code>.
</p>

<p>Uz jednako razmišljanje možemo prikazati i djelovanje operatora <code>and</code> i <code>or</code>.</p>

<p>Krenimo najprije na operator <code>and</code>.<br>
<code>'0' and '0' = '0'</code><br>
<code>'0' and '1' = '0'</code><br>
<code>'0' and 'U' = '0'</code><br>
<code>'1' and '0' = '0'</code><br>
<code>'1' and '1' = '1'</code><br>
<code>'1' and 'U' = 'U'</code><br>
<code>'U' and '0' = '0'</code><br>
<code>'U' and '1' = 'U'</code><br>
<code>'U' and 'U' = 'U'</code>
</p>

<p>Primijetite: <code>'0' and 'U' = '0'</code> jer neovisno o stvarnoj vrijednosti koja se krije iza <code>'U'</code>,
   prema aksiomima Booleove algebre, ako je jedan od argumenata logičkog I logička nula, rezultat je logička nula neovisno o
   drugom argumentu. Međutim, <code>'1' and 'U' = 'U'</code> jer rezultat ovisi o stvarnoj vrijednosti drugog argumenta.</p>

<p>Slično imamo i za operator <code>or</code>.<br>
<code>'0' or '0' = '0'</code><br>
<code>'0' or '1' = '1'</code><br>
<code>'0' or 'U' = 'U'</code><br>
<code>'1' or '0' = '1'</code><br>
<code>'1' or '1' = '1'</code><br>
<code>'1' or 'U' = '1'</code><br>
<code>'U' or '0' = 'U'</code><br>
<code>'U' or '1' = '1'</code><br>
<code>'U' or 'U' = 'U'</code><br>
</p>


